// -*- compile-command: "go build godate.go" -*-

// Example program for time.Strftime().

// Copyright (c) 2012 Juergen Nickelsen <ni@w21.org>. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.


package main

import "os"
import "time"
import w21time "w21.org/time"
import "fmt"

// usage: godate +dateformat [year [month [day [hour [minute [second]]]]]]
func main() {
        var date time.Time
        argc := len(os.Args)
        var datec [6]int                // Y m d H M S
//         var tzone string
        format := w21time.StandardFormat
        haveDate := false

        if argc > 1 {
                nextArg := 1
                if len(os.Args[nextArg]) >= 1 || os.Args[nextArg][0] == '+' {
                        format = os.Args[nextArg][1:]
                        nextArg++
                }
                dci := 0
                for ; nextArg < argc && dci < len(datec); nextArg++ {
                        haveDate = true
                        var err error
                        _, err = fmt.Sscanf(os.Args[nextArg], "%d", &datec[dci])
                        dci++
                        if err != nil {
                                fmt.Fprintf(os.Stderr, "arg[%d] == %s: %v\n",
                                        nextArg, os.Args[nextArg], err)
                                os.Exit(2)
                        }
                }
//                 if nextArg < argc {
//                         tzone = os.Args[nextArg]
//                 }
                date = time.Date(datec[0], time.Month(datec[1]), datec[2],
                        datec[3], datec[4], datec[5], 0, time.Now().Location())
        }
        if !haveDate {
                date = time.Now()
        }
        
        fmt.Println(w21time.Strftime(format, date))
}

// EOF
