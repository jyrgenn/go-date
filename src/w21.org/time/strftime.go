// -*- compile-command: "go build godate.go" -*-

// Start of a strftime() implementation. It tries to be close to POSIX; locales
// and modifiers (E, O, field width, plus, minus) are not supported, but
// gracefully ignored. All conversion specifiers should otherwise work as
// specified in POSIX. Not rigorously tested yet.

// Copyright (c) 2012 Juergen Nickelsen <ni@w21.org>. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.

package time

import "time"
import "fmt"
import "bytes"

// A time format specifier for the combined date and time according to ISO 8601
const ISO8601LocalFormat = "%Y-%m-%dT%H:%M:%S%z"

//  A time format specifier for date and time that may be convenient to read
const StandardFormat = "%Y-%m-%d %H:%M:%S %z %Z"

// return the beginning of the year in t
func beginningOfYear(t time.Time) time.Time {
        return time.Date(t.Year(), 1, 1, 0, 0, 0, 0, t.Location())
}

// day of the year, counting from 0
func dayOfYear(t time.Time) int {
        return int(t.Unix()-beginningOfYear(t).Unix()) / 86400
}

// Return the day of the year (starting at 0) in which week 1 starts, with
// wday as the first day of the week. All days in a new year preceding the
// first wday are considered to be in week 0.
func startOfWeek1(t time.Time, wday time.Weekday) int {
        jan1 := int(beginningOfYear(t).Weekday()) - int(wday)
        if jan1 < 0 {
                jan1 += 7
        }
        return (7 - jan1) % 7
}

// week number of the year as a decimal number string [0,53]. The first `wday'
// of January is the first day of week 1; days in the new year before this are
// in week 0
func weekNumber(t time.Time, wday time.Weekday) string {
        yday := dayOfYear(t)
        w1day := startOfWeek1(t, wday)
        if yday < w1day {
                return "0"
        }
        return fmt.Sprintf("%d", (yday-w1day)/7+1)
}

// A strftime() implementation close to POSIX; locales and modifiers (E, O,
// field width, plus, minus) are not supported, but gracefully ignored. All
// conversion specifiers should otherwise work as specified in POSIX.
func Strftime(format string, t time.Time) string {
        //fmt.Println("Strftime(", format, t, ")")
        var rbuf bytes.Buffer // result buffer
        inSpec := false       // in a specifier after reading '%'
        for _, r := range format {
                if !inSpec {
                        if r == '%' {
                                inSpec = true
                        } else {
                                rbuf.WriteRune(r)
                        }
                        continue
                }
                inSpec = false // true for most characters, at least
                var s string
                switch r {
                case 'E', 'O', '+', '-', '0', '1', '2', '3', '4', '5',
                        '6', '7', '8', '9': // modifiers, ignored
                        inSpec = true // still in a specifier
                case 'a': // locale's abbreviated weekday name
                        s = t.Weekday().String()[:3]
                case 'A': // locale's full weekday name
                        s = t.Weekday().String()
                case 'b', 'h': // locale's abbreviated month name
                        s = t.Month().String()[:3]
                case 'B': // locale's full month name
                        s = t.Month().String()
                case 'c': // locale's appropriate date and time representation
                        s = Strftime(StandardFormat, t)
                case 'C':
                        // Replaced by the year divided by 100 and truncated to
                        // an integer, as a decimal number
                        s = fmt.Sprintf("%02d", t.Year()/100)
                case 'd': // day of the month as a decimal number [01,31]
                        s = fmt.Sprintf("%02d", t.Day())
                case 'D': // Equivalent to %m / %d / %y
                        s = Strftime("%m/%d/%y", t)
                case 'e':
                        // day of the month as a decimal number [1,31]; a
                        // single digit is preceded by a space
                        s = fmt.Sprintf("%2d", t.Month())
                case 'f': // Microsecond as a decimal number [000000,999999]
                        s = fmt.Sprintf("%06d", t.Nanosecond()/1000)
                case 'F':
                        // Equivalent to %+4Y-%m-%d if no flag and no minimum
                        // field width are specified (sez POSIX)
                        s = Strftime("%Y-%m-%d", t)
                case 'g':
                        // last 2 digits of the week-based year as a decimal
                        // number [00,99]
                        year, _ := t.ISOWeek()
                        s = fmt.Sprintf("%02d", year%100)
                case 'G':
                        // week-based year as a decimal number (for example,
                        // 1977)
                        year, _ := t.ISOWeek()
                        s = fmt.Sprintf("%02d", year)
                // for 'h' see 'b'
                case 'H': // hour (24-hour clock) as a decimal number [00,23]
                        s = fmt.Sprintf("%02d", t.Hour())
                case 'I': // hour (12-hour clock) as a decimal number [01,12]
                        hour := t.Hour() % 12
                        if hour == 0 {
                                hour = 12
                        }
                        s = fmt.Sprintf("%02d", hour)
                case 'j': // day of the year as a decimal number [001,366]
                        s = fmt.Sprintf("%03d", dayOfYear(t)+1)
                case 'm': // month as a decimal number [01,12]
                        s = fmt.Sprintf("%02d", int(t.Month()))
                case 'M': // minute as a decimal number [00,59]
                        s = fmt.Sprintf("%02d", t.Minute())
                case 'n': // a <newline>
                        s = "\n"
                case 'p': // locale's equivalent of either AM or PM.
                        if t.Hour() < 12 {
                                s = "AM"
                        } else {
                                s = "PM"
                        }
                case 'r': // the time in a.m. and p.m. notation
                        s = Strftime("%I:%M:%S %p", t)
                case 'R': // time in 24-hour notation
                        s = fmt.Sprintf("%02d:%02d", t.Hour(), t.Minute())
                case 'S': // Second as a decimal number [00,61].
                        s = fmt.Sprintf("%02d", t.Second())
                case 't': // a <tab>
                        s = "\t"
                case 'T': // the time ( %H : %M : %S )
                        s = fmt.Sprintf("%02d:%02d:%02d",
                                t.Hour(), t.Minute(), t.Second())
                case 'u':
                        // the weekday as a decimal number [1,7], with 1
                        // representing Monday
                        day := t.Weekday()
                        if day == 0 {
                                day = 7
                        }
                        s = fmt.Sprintf("%d", day)
                case 'U':
                        // week number of the year as a decimal number [00,53]. 
                        // The first Sunday of January is the first day of week
                        // 1; days in the new year before this are in week 0
                        s = weekNumber(t, time.Sunday)
                case 'v': // (BSD extension? was in OS X)
                        s = Strftime("%e-%b-%Y", t)
                case 'V':
                        // (ISO week number) week number of the year (Monday as
                        // the first day of the week) as a decimal number
                        // [01,53]. If the week containing 1 January has four
                        // or more days in the new year, then it is considered
                        // week 1. Otherwise, it is the last week of the
                        // previous year, and the next week is week 1. Both
                        // January 4th and the first Thursday of January are
                        // always in week 1
                        _, week := t.ISOWeek()
                        s = fmt.Sprintf("%d", week)
                case 'w':
                        // weekday as a decimal number [0,6], with 0
                        // representing Sunday
                        s = fmt.Sprintf("%d", t.Weekday())
                case 'W':
                        // week number of the year as a decimal number [00,53]. 
                        // The first Monday of January is the first day of week
                        // 1; days in the new year before this are in week 0
                        s = weekNumber(t, time.Monday)
                case 'x': // locale's appropriate date representation.
                        s = Strftime("%Y-%m-%d", t)
                case 'X': // locale's appropriate time representation.
                        s = Strftime("%H:%M:%S", t)
                case 'y':
                        // last two digits of the year as a decimal number
                        // [00,99]
                        s = fmt.Sprintf("%02d", t.Year()%100)
                case 'Y': // year as a decimal number (for example, 1997)
                        s = fmt.Sprintf("%04d", t.Year())
                case 'z':
                        // offset from UTC in the ISO 8601:2000 standard format
                        // ( +hhmm or -hhmm ), or by no characters if no
                        // timezone is determinable. For example, "-0430" means
                        // 4 hours 30 minutes behind UTC (west of Greenwich). 
                        // [CX] If tm_isdst is zero, the standard time offset
                        // is used. If tm_isdst is greater than zero, the
                        // daylight savings time offset is used. If tm_isdst is
                        // negative, no characters are returned
                        _, offset := t.Zone()
                        min := offset / 60 % 60
                        hour := offset / 3600
                        s = fmt.Sprintf("%+03d%02d", hour, min)
                case 'Z':
                        // timezone name or abbreviation, or by no bytes if no
                        // timezone information exists
                        name, _ := t.Zone()
                        s = name
                case '%': // A literal '%' character.
                        s = "%"
                default:
                        s = fmt.Sprintf("%%!<Strftime:'%c'invalid>", r)
                }
                rbuf.WriteString(s)
        }
        //fmt.Println(rbuf)
        return rbuf.String()
}

// EOF
